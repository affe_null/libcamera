/* SPDX-License-Identifier: LGPL-2.1-or-later */
/*
 * camss.cpp - Pipeline handler for the CAMSS ISP found on Qualcomm SoCs
 */

#include <algorithm>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>

#include <libcamera/base/log.h>

#include <libcamera/camera.h>
#include <libcamera/control_ids.h>
#include <libcamera/formats.h>
#include <libcamera/request.h>
#include <libcamera/stream.h>

#include "libcamera/internal/camera.h"
#include "libcamera/internal/camera_sensor.h"
#include "libcamera/internal/device_enumerator.h"
#include "libcamera/internal/media_device.h"
#include "libcamera/internal/pipeline_handler.h"
#include "libcamera/internal/v4l2_subdevice.h"
#include "libcamera/internal/v4l2_videodevice.h"


namespace libcamera {

LOG_DEFINE_CATEGORY(CAMSS)

class PipelineHandlerCamss;

class VfeLine
{
public:
	VfeLine(std::unique_ptr<V4L2Subdevice> subdev, std::unique_ptr<V4L2VideoDevice> video)
		: subdev_(std::move(subdev)), video_(std::move(video))
	{}
	int start();
	void stop();

	bool setupLinks(unsigned int i);

	int connectCamera(const MediaEntity *csiphy, V4L2SubdeviceFormat *format);

	int setFormat(const PixelFormat &pixelFormat, const Size &size);

	int exportBuffers(unsigned int count, std::vector<std::unique_ptr<FrameBuffer>> *buffers)
	{
		return video_->exportBuffers(count, buffers);
	}

	int queueBuffer(FrameBuffer *buffer)
	{
		return video_->queueBuffer(buffer);
	}

private:
	std::unique_ptr<V4L2Subdevice> subdev_;
	std::unique_ptr<V4L2VideoDevice> video_;
	std::vector<std::unique_ptr<V4L2Subdevice>> path_;
};

class CamssCameraData : public Camera::Private
{
public:
	CamssCameraData(PipelineHandler *pipe)
		: Camera::Private(pipe)
	{
		streams_.resize(1);
	}

	std::unique_ptr<CameraSensor> sensor_;
	std::unique_ptr<V4L2Subdevice> csiphy_;

	std::vector<Stream> streams_;
};

class CamssCameraConfiguration : public CameraConfiguration
{
public:
	CamssCameraConfiguration(CamssCameraData *data)
		: CameraConfiguration(), data_(data)
	{}

	Status validate() override;

	V4L2SubdeviceFormat sensorFormat_;

private:
	CamssCameraData *data_;
};

class PipelineHandlerCamss : public PipelineHandler
{
public:
	PipelineHandlerCamss(CameraManager *manager);

	std::unique_ptr<CameraConfiguration> generateConfiguration(Camera *camera,
								   Span<const StreamRole> roles) override;
	int configure(Camera *camera, CameraConfiguration *config) override;

	int exportFrameBuffers(Camera *camera, Stream *stream,
			       std::vector<std::unique_ptr<FrameBuffer>> *buffers) override;

	int start(Camera *camera, const ControlList *controls) override;
	void stopDevice(Camera *camera) override;

	int queueRequestDevice(Camera *camera, Request *request) override;

	bool match(DeviceEnumerator *enumerator) override;

private:
	CamssCameraData *cameraData(Camera *camera)
	{
		return static_cast<CamssCameraData *>(camera->_d());
	}

	void bufferReady(FrameBuffer *buffer);

	MediaDevice *device_;
	std::vector<VfeLine> pixLines_;
};

int VfeLine::start()
{
	int ret = video_->importBuffers(1);
	if (ret)
		return ret;

	return video_->streamOn();
}

void VfeLine::stop()
{
	video_->streamOff();
	video_->releaseBuffers();
}

int VfeLine::connectCamera(const MediaEntity *csiphy, V4L2SubdeviceFormat *format)
{
	const MediaEntity *csid = path_.back()->entity();

	bool connected = false;
	for (MediaLink *link : csid->pads()[0]->links()) {
		if (link->source()->entity() == csiphy) {
			link->setEnabled(true);
			connected = true;
		} else {
			link->setEnabled(false);
		}
	}

	if (!connected) {
		LOG(CAMSS, Error) << "No link between " << csiphy->name()
				  << " and " << csid->name();
		return -EPIPE;
	}

	for (auto &sd : path_) {
		int ret = sd->setFormat(0, format);
		if (ret)
			return ret;
	}

	return subdev_->setFormat(0, format);
}

int VfeLine::setFormat(const PixelFormat &pixelFormat, const Size &size)
{
	Rectangle compose(size);
	int ret = subdev_->setSelection(0, V4L2_SEL_TGT_COMPOSE, &compose);
	if (ret)
		return ret;

	V4L2DeviceFormat fmt{};
	fmt.fourcc = video_->toV4L2PixelFormat(pixelFormat);
	fmt.size = size;

	return video_->setFormat(&fmt);
}

/*
 * Link the VFE to a CSID, saving the CSID and all intermediate entities
 * to allow setting the format later. The number of CSIDs in the pipeline
 * is assumed to be greater than or equal to the number of VFEs.
 */
bool VfeLine::setupLinks(unsigned int i)
{
	std::string csidName = "msm_csid" + std::to_string(i);

	MediaPad *pad = subdev_->entity()->pads()[0];

	do {
		if (i >= pad->links().size())
			return false;

		MediaLink *link = pad->links()[i];
		link->setEnabled(true);
		pad = link->source()->entity()->pads()[0];
		path_.push_back(std::make_unique<V4L2Subdevice>(pad->entity()));
		int ret = path_.back()->open();
		if (ret) {
			LOG(CAMSS, Error)
				<< "Failed to open " << path_.back()->deviceNode()
				<< ": " << strerror(-ret);
			return false;
		}
	} while (pad->entity()->name() != csidName);

	return true;
}

namespace {

const std::vector<unsigned int> inputFormats = {
	MEDIA_BUS_FMT_UYVY8_2X8,
	MEDIA_BUS_FMT_VYUY8_2X8,
	MEDIA_BUS_FMT_YUYV8_2X8,
	MEDIA_BUS_FMT_YVYU8_2X8,
	MEDIA_BUS_FMT_SBGGR10_1X10,
	MEDIA_BUS_FMT_SGBRG10_1X10,
	MEDIA_BUS_FMT_SGRBG10_1X10,
	MEDIA_BUS_FMT_SRGGB10_1X10,
};

const std::vector<PixelFormat> outputFormats = {
	formats::NV12,
	formats::NV21,
	formats::NV16,
	formats::NV61,
};

};

CameraConfiguration::Status CamssCameraConfiguration::validate()
{
	const CameraSensor *sensor = data_->sensor_.get();
	Status status = Valid;

	if (config_.empty())
		return Invalid;

	if (config_.size() > 1) {
		config_.resize(1);
		status = Adjusted;
	}

	if (std::find(outputFormats.begin(), outputFormats.end(), config_[0].pixelFormat) ==
	    outputFormats.end()) {
		config_[0].pixelFormat = outputFormats[0];
		status = Adjusted;
	}

	if (config_[0].bufferCount != 1) {
		config_[0].bufferCount = 1;
		status = Adjusted;
	}

	sensorFormat_ = sensor->getFormat(inputFormats, config_[0].size);

	return status;
}

PipelineHandlerCamss::PipelineHandlerCamss(CameraManager *manager)
	: PipelineHandler(manager)
{
}

std::unique_ptr<CameraConfiguration>
PipelineHandlerCamss::generateConfiguration(Camera *camera, Span<const StreamRole> roles)
{
	CamssCameraData *data = cameraData(camera);
	std::unique_ptr<CameraConfiguration> config =
		std::make_unique<CamssCameraConfiguration>(data);

	if (roles.empty())
		return config;

	if (roles.size() > 1) {
		LOG(CAMSS, Error) << "Only one stream is supported";
		return nullptr;
	}

	Size minSize = { 1, 1 };
	Size maxSize = data->sensor_->resolution();

	std::map<PixelFormat, std::vector<SizeRange>> formats;

	for (const PixelFormat &format : outputFormats) {
		formats[format] = { { minSize, maxSize } };
	}

	StreamConfiguration cfg{ StreamFormats{ formats } };

	switch (roles[0]) {
	case StreamRole::StillCapture:
	case StreamRole::Viewfinder:
	case StreamRole::VideoRecording:
		cfg.size = roles[0] == StreamRole::StillCapture
			 ? data->sensor_->resolution()
			 : data->sensor_->sizes(data->sensor_->mbusCodes()[0])[0];
		cfg.pixelFormat = formats::NV12;
		cfg.bufferCount = 1;
		cfg.setStream(&data->streams_[0]);
		break;
		/* \todo implement raw capture */
	default:
		LOG(CAMSS, Error) << "Stream role " << roles[0] << " not supported";
		return nullptr;
	}

	config->addConfiguration(cfg);

	config->validate();

	return config;
}

int PipelineHandlerCamss::configure(Camera *camera, CameraConfiguration *c)
{
	CamssCameraConfiguration *config =
		static_cast<CamssCameraConfiguration *>(c);
	CamssCameraData *data = cameraData(camera);
	int ret;

	V4L2SubdeviceFormat format = config->sensorFormat_;
	ret = data->sensor_->setFormat(&format);
	if (ret)
		return ret;

	ret = data->csiphy_->setFormat(0, &format);
	if (ret)
		return ret;

	ret = pixLines_[0].connectCamera(data->csiphy_->entity(), &format);
	if (ret)
		return ret;

	return pixLines_[0].setFormat((*config)[0].pixelFormat, (*config)[0].size);
}

int PipelineHandlerCamss::exportFrameBuffers([[maybe_unused]] Camera *camera, Stream *stream,
					     std::vector<std::unique_ptr<FrameBuffer>> *buffers)
{
	unsigned int count = stream->configuration().bufferCount;

	return pixLines_[0].exportBuffers(count, buffers);
}

int PipelineHandlerCamss::start([[maybe_unused]] Camera *camera, [[maybe_unused]] const ControlList *controls)
{
	return pixLines_[0].start();
}

void PipelineHandlerCamss::stopDevice([[maybe_unused]] Camera *camera)
{
	pixLines_[0].stop();
}

int PipelineHandlerCamss::queueRequestDevice([[maybe_unused]] Camera *camera, Request *request)
{
	for (auto &[stream, buffer] : request->buffers()) {
		int ret = pixLines_[0].queueBuffer(buffer);
		if (ret)
			return ret;
	}

	return 0;
}

bool PipelineHandlerCamss::match(DeviceEnumerator *enumerator)
{
	DeviceMatch dm("qcom-camss");

	dm.add("msm_vfe0_pix");

	device_ = acquireMediaDevice(enumerator, dm);
	if (!device_)
		return false;

	/* Enumerate all available CSIPHYs to find connected cameras. */
	bool registered = false;
	for (unsigned int i = 0; ; ++i) {
		std::string entityName = "msm_csiphy" + std::to_string(i);
		std::unique_ptr<V4L2Subdevice> subdev =
			V4L2Subdevice::fromEntityName(device_, entityName);
		if (!subdev)
			break;

		MediaEntity *sensor = nullptr;
		for (MediaPad *pad : subdev->entity()->pads()) {
			if (!(pad->flags() & MEDIA_PAD_FL_SINK) || pad->links().empty())
				continue;

			sensor = pad->links()[0]->source()->entity();
			if (sensor->function() != MEDIA_ENT_F_CAM_SENSOR) {
				LOG(CAMSS, Debug) << "Ignoring non-sensor device "
						  << sensor->name()
						  << " connected to CSIPHY";
				continue;
			}
			break;
		}
		if (!sensor)
			continue;

		std::unique_ptr<CamssCameraData> data =
			std::make_unique<CamssCameraData>(this);

		data->sensor_ = std::make_unique<CameraSensor>(sensor);
		data->csiphy_ = std::move(subdev);

		std::set<Stream *> streams;
		std::transform(data->streams_.begin(), data->streams_.end(),
			       std::inserter(streams, streams.end()),
			       [](Stream &stream) { return &stream; });

		int ret = data->sensor_->init();
		if (ret) {
			LOG(CAMSS, Error)
				<< "Failed to initialize sensor "
				<< sensor->name() << ": " << strerror(-ret);
			return false;
		}

		ret = data->csiphy_->open();
		if (ret) {
			LOG(CAMSS, Error)
				<< "Failed to open " << data->csiphy_->deviceNode()
				<< ": " << strerror(-ret);
			return false;
		}

		const std::string &id = data->sensor_->id();
		std::shared_ptr<Camera> camera =
			Camera::create(std::move(data), id, streams);
		registerCamera(std::move(camera));
		registered = true;
	}

	/* Find available VFEs and link them to the CSIDs */
	for (unsigned int i = 0; ; ++i) {
		std::string entityName = "msm_vfe" + std::to_string(i) + "_pix";
		std::unique_ptr<V4L2Subdevice> subdev =
			V4L2Subdevice::fromEntityName(device_, entityName);
		if (!subdev)
			break;

		int ret = subdev->open();
		if (ret) {
			LOG(CAMSS, Error)
				<< "Failed to open " << subdev->deviceNode()
				<< ": " << strerror(-ret);
			return false;
		}

		MediaEntity *entity = nullptr;
		for (MediaPad *pad : subdev->entity()->pads()) {
			if (!(pad->flags() & MEDIA_PAD_FL_SINK)) {
				entity = pad->links()[0]->sink()->entity();
				break;
			}
		}
		if (!entity) {
			LOG(CAMSS, Error)
				<< "Could not find video device for "
				<< entityName;
			continue;
		}

		std::unique_ptr<V4L2VideoDevice> video =
			std::make_unique<V4L2VideoDevice>(entity);

		video->bufferReady.connect(this, &PipelineHandlerCamss::bufferReady);

		ret = video->open();
		if (ret) {
			LOG(CAMSS, Error)
				<< "Failed to open " << video->deviceNode()
				<< ": " << strerror(-ret);
			return false;
		}

		VfeLine &line = pixLines_.emplace_back(std::move(subdev), std::move(video));
		if (!line.setupLinks(i)) {
			LOG(CAMSS, Error)
				<< "Failed to link " << video->deviceNode()
				<< " to a CSID";
		}
	}

	return registered;
}

void PipelineHandlerCamss::bufferReady(FrameBuffer *buffer)
{
	Request *request = buffer->request();

	request->metadata().set(controls::SensorTimestamp,
				buffer->metadata().timestamp);

	completeBuffer(request, buffer);
	completeRequest(request);
}

REGISTER_PIPELINE_HANDLER(PipelineHandlerCamss)

} /* namespace libcamera */
